#%%
import os
import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from datetime import datetime

#%%
cwd = os.getcwd()
# google_data = os.path.join(cwd, 'manipulating-time-series-data-python/data/stock_data/google.csv')
google_data = 'data/stock_data/google.csv'
print("processing file:", google_data)
google = pd.read_csv(google_data)

#%%
print("=.=.=.=.=.=.=.")
print(google.info())
print("=.=.=.=.=.=.=.")

#%%
print(google.head())

#%%
google.Date = pd.to_datetime(google.Date)
print(google.info())


#%%
google.set_index('Date', inplace=True)
print(google.info())

#%%
google.Close.plot(title='Google Stock Price')
plt.tight_layout()
plt.show()

#%%

google['2015'].info()

#%%
google['2015-3': '2016-2'].info()


#%%
google.loc['2016-6-1', 'Close']

#%%
google.asfreq('D').info()

#%%
google.head()

#%%
google = google.asfreq('B')
google.info()

#%%
google[google.Close.isnull()]

#%%


# bokeh basics
from bokeh.plotting import figure
from bokeh.io import show, output_notebook

# Create a blank figure with labels
p = figure(plot_width = 600, plot_height = 600,
           title = 'Example Glyphs',
           x_axis_label = 'X', y_axis_label = 'Y')

# Example data
squares_x = [1, 3, 4, 5, 8]
squares_y = [8, 7, 3, 1, 10]
circles_x = [9, 12, 4, 3, 15]
circles_y = [8, 4, 11, 6, 10]

# Add squares glyph
p.square(squares_x, squares_y, size = 12, color = 'navy', alpha = 0.6)
# Add circle glyph
p.circle(circles_x, circles_y, size = 12, color = 'red')

# Set to output the plot in the notebook
output_notebook()
# Show the plot
show(p)