#%%
file = 'E:\\datasets\\data\\osm\\NewYork.osm'
with open(file, encoding='utf-8') as f:
    data_ny = f.readlines()


#%%
import os
import math
size = os.path.getsize(file) / math.pow(1024, 3)
print("{:.3} GB".format(size))

#%%
[x.strip() for x in data_ny[230:235]]

#%%

print(data_ny[240])

print(data_ny[240].strip().split('"'))

#%%
tags_ny = [line.strip().split('"') for line in data_ny if "tag" in line]
tags_ny[:3]

#%%
tags_ny[1:100000:10000]


#%%
from operator import itemgetter
extract_tag = itemgetter(1, 3)


#%%

extract_tag([1, 2, 3, 4])


#%%
tags_ny = [extract_tag(line.split('"')) for line in data_ny if 'tag' in line]
len(tags_ny)

#%%
tags_ny[1:100000:10000]

#%%
from collections import Counter
c = Counter(tags_ny)

#%%
c.most_common(5)