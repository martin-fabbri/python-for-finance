#%%
import dask.bag as db
from operator import itemgetter
from dask.diagnostics import ProgressBar

#%%
first_list = [1, 4, 10, 20]
bag = db.from_sequence(first_list, npartitions=1)
bag


#%%
# transformations
bag.map(lambda x: x + 1)

#%%
bag.map(lambda x: x + 1).compute()
bag

#%%
bag.fold(lambda acc, e: acc + e, initial=0).compute()


#%%
bag.sum().compute()

#%%
bag.foldby(lambda x: x % 2 == 0, lambda x, y: x + y, initial=0).compute()

#%%
csvs = 'E:\\datasets\\data\\osm\\southamerica\\*'
data = db.read_text(csvs)
data

#%%
extract_tag = itemgetter(1, 3)

#%%
task = data.filter(
    lambda x: 'tag' in x
).foldby(
    lambda x: extract_tag(x.split('"')),
    lambda x, _: x + 1, 0
).topk(10, itemgetter(1))
task

#%%
with ProgressBar():
    counts = task.compute()

counts