#%%
import os
import math

dir = 'E:\\datasets\\data\\osm\\southamerica'
size = sum(os.path.getsize(os.path.join(dir, f)) for f in os.listdir(dir)) / math.pow(1024, 3)
print("{:.3} GB".format(size))

#%%
lines = []
for file in os.listdir(dir):
    with open(os.path.join(dir, file), encoding='utf-8') as f:
        lines.append(f.readlines())
lines